---
title: Home
---

## Bienvenue

<img src="/assets/images/haskell-logo.png" style="float: right; margin: 10px;" />

Bienvenue sur la version française de mon site.
La version anglaise se trouve [ici](/en).

Vous trouverez ci-dessous la liste de mes derniers articles.

## Billets :

$partial("templates/post-list.html")$

... Vous trouverez l'intégralité de mes publications dans les [archives](/$language$/archive).
