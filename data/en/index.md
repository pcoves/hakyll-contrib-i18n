---
title: Home
---

## Welcome

<img src="/assets/images/haskell-logo.png" style="float: right; margin: 10px;" />

Welcome on the English version on my site.
The French one can be found [here](/fr).

I've reproduced a list of recent posts here for your reading pleasure :

## Posts :

$partial("templates/post-list.html")$

... or you can find more in the [archive](/$language$/archive).
