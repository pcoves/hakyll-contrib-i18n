function venv {
    # Deactivate the virual environment if there's one alredy activated.
    [[ `command -v deactivate` ]] && deactivate

    # Activate the virual environment if there is one to activate.
    [[ -f ./venv/bin/activate ]]  && source ./venv/bin/activate
}

function cd {
    # Call the builtin `cd` and pass its arguments.
    builtin cd "$@"

    # (De)Activate virual environment implicitely.
    venv
}
