#  ____        _ _     _
# | __ ) _   _(_) | __| |
# |  _ \| | | | | |/ _` |
# | |_) | |_| | | | (_| |
# |____/ \__,_|_|_|\__,_|

FROM registry.gitlab.com/pcoves/hakyll-contrib-i18n:dependencies as build
ADD . .
RUN cabal install --install-method=copy --overwrite-policy=always

#  ____  _     _              __       _ _ 
# |  _ \(_)___| |_ _ __ ___  / _|_   _| | |
# | | | | / __| __| '__/ _ \| |_| | | | | |
# | |_| | \__ \ |_| | | (_) |  _| |_| | | |
# |____/|_|___/\__|_|  \___/|_|  \__,_|_|_|
                                       
FROM debian:latest
COPY --from=build /root/.cabal/bin/haki18nll /root/.local/bin/haki18nll
ENTRYPOINT ["/root/.local/bin/haki18nll"]

# #  ____  _     _             _
# # |  _ \(_)___| |_ _ __ ___ | | ___  ___ ___
# # | | | | / __| __| '__/ _ \| |/ _ \/ __/ __|
# # | |_| | \__ \ |_| | | (_) | |  __/\__ \__ \
# # |____/|_|___/\__|_|  \___/|_|\___||___/___/

# # One day, maybe.

# FROM gcr.io/distroless/base
# COPY --from=build /root/.cabal/bin/haki18nll /root/.local/bin/haki18nll
# ENTRYPOINT ["/root/.local/bin/haki18nll"]
